package com.techuniversity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLSyntaxErrorException;

public class Users {

    private static JSONObject data=null;

    public static void cargarUsers() throws FileNotFoundException {
        try (InputStream resourceAsStream = Users.class.getClassLoader().getResourceAsStream("user.json")){
            InputStreamReader isr = new InputStreamReader(resourceAsStream);
            BufferedReader br = new BufferedReader(isr);
            String line;
            String str="";
            while((line = br.readLine())!= null){
                str +=line;
            }
            data = new JSONObject(str);
            System.out.println(data);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public static JSONObject getUser(String id) throws JSONException, FileNotFoundException {
        if(data == null){
            cargarUsers();
        }
        JSONObject selUser = null;
        JSONArray users = data.getJSONArray("users");
        for(int i=0; i< users.length();i++){
            JSONObject user = users.getJSONObject(i);
            String idUser = user.getString("userId");
            if(idUser.equals(id)){
                selUser = user;
                break;
            }
        }
        //selUser = users.getJSONObject(userId);
        return selUser;
    }

}
